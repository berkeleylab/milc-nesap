#!/bin/bash
# Script to parse MILC output
#

# Parse the command line parameters
verbose=false
iter_thresh=0
force_result=false
while getopts "vt:f" opt; do
  case $opt in 
    v) 
      verbose=true;;
    t) 
      iter_thresh=$OPTARG;;
    f) 
      force_result=true;;
    :)
      echo "Option -$OPTARG requires an arument" >&2; exit;;
    \?)
      echo "ERROR: Invalid option: -$OPTARG" >&2; exit;;
  esac
done
shift $((OPTIND -1))
if $verbose; then echo "Iteration threshold = $iter_thresh"; fi

# The remaining command line is a list of files to be parsed
first_time=true;
files_parsed=0
echo "Parsing $@"
for file in $@; do
  if [ -e $file ]; then
    exec < $file
    ((files_parsed++))
  else
    echo "***WARNING***: $file doesn't exist" >&2
    continue
  fi

  start=false
  i=0;
  while read -a fields; do
    if [ ${#fields[@]} -eq 0 ]; then # empy line
      continue
    fi
    case ${fields[0]} in
      "Machine") # Number of MPI ranks
        num_ranks[$i]=${fields[5]}
        if $verbose; then echo "${num_ranks[$i]} MPI ranks"; fi
        ;;
      "WARMUPS") # WARMUPS COMPLETED
        if $verbose; then echo "WARMUPS COMPLETED, starting calculation"; fi
        start=true
        total[$i]=0
        cg[$i]=0
        fforce[$i]=0
        gforce[$i]=0
        mem[$i]=0
        cg_flops[$i]=0; cg_iters[$i]=0
        gf_flops[$i]=0; gf_iters[$i]=0
        ff_flops[$i]=0; ff_iters[$i]=0
        ;;
      "RUNNING") # RUNNING COMPLETED
        if $verbose; then echo "RUNNING COMPLETED, stopping calculation"; fi
        start=false
        ;;
      "TOTAL_TIME")
        total[$i]=${fields[1]};;
      "CG_TIME")
        cg[$i]=${fields[1]};;
      "FF_TIME")
        fforce[$i]=${fields[1]};;
      "GF_TIME")
        gforce[$i]=${fields[1]};;
      "Approximate")			# this is actually the last field in a stanza
        mem[$i]=${fields[4]}
        (( i++ ));;
    esac

    if $start; then
      case ${fields[0]} in
        "CONGRAD5:") # CG FLOPS 
          iters=${fields[11]}
          if [ $iters -gt $iter_thresh ]; then
            if $verbose; then echo "${fields[@]}"; fi
            flops=`awk -v a="${fields[14]}" 'BEGIN{print (a * 1)}'`
            cg_flops[$i]=`echo "${cg_flops[$i]} + $iters * $flops" | bc`
            (( cg_iters[$i] += iters ))
          fi
          ;;
        "GFTIME:") # GF FLOPS 
          if $verbose; then echo "${fields[@]}"; fi
          flops=`awk -v a="${fields[7]}" 'BEGIN{print (a * 1)}'`
          gf_flops[$i]=`echo "${gf_flops[$i]} + $flops" | bc`
          (( gf_iters[$i] += 1 ))
          ;;
        "FFTIME:") # FF FLOPS 
          if $verbose; then echo "${fields[@]}"; fi
          flops=`awk -v a="${fields[14]}" 'BEGIN{print (a * 1)}'`
          ff_flops[$i]=`echo "${ff_flops[$i]} + $flops" | bc`
          (( ff_iters[$i] += 1 ))
          ;;
      esac
    fi
  done # end of reading fields


  # print results
  if $first_time; then
    printf "%-12s%-12s%-12s%-12s%-12s%-12s%-12s%-12s\n" \
      "TOTAL_TIME" "CG_TIME" "FF_TIME" "GF_TIME" "Memory(MB)" \
      "CG(GF/s)" "FF(GF/s)" "GF(GF/s)"
    first_time=false
  fi

  if $force_result && [ $i -eq 0 ]; then
    i=1
  fi
  for ((j=0; j < i; j++)); do
    if [ ${cg_iters[$j]} -gt 0 ] || [ ${gf_iters[$j]} -gt 0 ] || [ ${ff_iters[$j]} -gt 0 ]; then
      if [ ${cg_iters[$j]} -gt 0 ]; then
        cg_flops[$j]=`echo "${num_ranks[$j]} * ${cg_flops[$j]} / ${cg_iters[$j]} / 1000" | bc -l`
      fi
      if [ ${gf_iters[$j]} -gt 0 ]; then
        gf_flops[$j]=`echo "${num_ranks[$j]} * ${gf_flops[$j]} / ${gf_iters[$j]} / 1000" | bc -l`
      fi
      if [ ${ff_iters[$j]} -gt 0 ]; then
        ff_flops[$j]=`echo "${num_ranks[$j]} * ${ff_flops[$j]} / ${ff_iters[$j]} / 1000" | bc -l`
      fi
      printf "%-12s%-12s%-12s%-12s%-12s%-12.3f%-12.3f%-12.3f\n" \
        ${total[$j]} ${cg[$j]} ${fforce[$j]} ${gforce[$j]} ${mem[$j]} \
        ${cg_flops[$j]} ${ff_flops[$j]} ${gf_flops[$j]}
    fi
  done

done # end of reading files

