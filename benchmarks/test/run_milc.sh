#!/bin/bash
#SBATCH --account=nstaff
#SBATCH --partition=debug
#SBATCH --constraint="haswell"
#SBATCH --nodes=1
#SBATCH --time=00:30:00
#SBATCH --job-name=milc
#SBATCH --wait-all-nodes=1
num_nodes=$SLURM_JOB_NUM_NODES
ht_node=2				# needs to match above
c_node=32				# default cores per node
t_node=$(( c_node * ht_node ))          # total threads per node
t_total=$(( num_nodes * t_node ))       # total threads

# support for IPM
export IPM_REPORT=full
export IPM_LOG=full
export IPM_LOGDIR=.

# Set defalt problem size
nx=16
ny=16
nz=16
nt=16

exe=su3_rhmd_hisq
build=false
debug=false
brief=true
nohuge=true
ht=1			# default hyperthreads per core
m=0 			# default numa memory mode
n=8			# default number of ranks per node
t=0			# derive threads
steps=0;		# used to set PROF_NUM_ITERS
while [ "$#" -ge 1 ] ; do
  case "$1" in
    "--help" ) 	          shift 1;;
    "--build" )           build=true; shift 1;;
    "--debug" )           debug=true; shift 1;;
    "--brief" )           brief=true; shift 1;;
    "--nohuge" )          nohuge=true; shift 1;;
    "--exe" )             exe=$2; shift 2;;
    "-s" | "--steps" )    steps=$2; shift 2;;
    "-n" | "--ranks" )    n=$2; shift 2;;
    "-t" | "--threads" )  t=$2; shift 2;;
    "-ht" )  		  ht=$2; shift 2;;
    "-m" )  		  m=$2; shift 2;;
    "-nx" )  		  nx=$2; shift 2;;
    "-ny" )  		  ny=$2; shift 2;;
    "-nz" )  		  nz=$2; shift 2;;
    "-nt" )  		  nt=$2; shift 2;;
    *)                    shift 1; continue;;
  esac
done
(( n *= num_nodes ))	# Total number of ranks

if [ $nohuge == "true" ]; then
  module unload craype-hugepages2M
  echo "NOT USING craype-hugepages"
else
  module load craype-hugepages2M
  echo "USING craype-hugepages"
fi

# set the executable
echo "USING exe=$exe"

if [ $steps -gt 0 ]; then
  export NERSC_NUM_STEPS=$steps
  echo "USING NERSC_NUM_STEPS=$NERSC_NUM_STEPS"
fi

# check for hyper threads
if [ $ht -gt $ht_node ]; then
  echo "ERROR: too many hyperthreads specified, exiting"
  exit
fi

dimension=${nx}x${ny}x${nz}x${nt}
checklat=$dimension.chklat
if [ $build == "true" ]; then
  . milc_build.sh
  warms=40
  trajecs=0
  #save_cmd="save_parallel $checklat"
  save_cmd="save_serial $checklat"
  reload_cmd="continue"
  echo "Building lattice of dimension $dimension"
else
  if [ $brief == "true" ]; then
    echo "USING milc_in_brief.sh"
    . milc_in_brief.sh
  else
    . milc_in.sh
  fi
  warms=0
  trajecs=2
  save_cmd="forget"
  reload_cmd="reload_parallel $checklat"
fi

export OMP_PLACES=threads
export OMP_PROC_BIND=spread
c=$(( t_total / n ))		# cores per rank
if [ $t -eq 0 ]; then		# t not set on command line
  t=$(( c / ht_node ))
  if [ $ht -gt 1 ]; then	# use hyperthreading
    export OMP_PLACES=cores"(${t})"
    (( t *= ht ))
    export OMP_PROC_BIND=close
  fi
fi
if [ $t -gt $c ]; then 	# t set, but check for sanity
  echo "ERROR: too many threads per rank specified, $t > $c, exiting"
  exit
fi 
export OMP_NUM_THREADS=$t

echo    "Running MILC, lattice $dimension"
echo -n "OMP_PLACES=$OMP_PLACES; "
echo -n "OMP_PROC_BIND=$OMP_PROC_BIND; "
echo -n "OMP_NUM_THREADS=$OMP_NUM_THREADS; "
command="srun -n $n -c $c --cpu_bind=cores $EXE_PREFIX /tmp/$exe"
echo "$command"
if [ $debug == "false" ]; then 
  sbcast -f $exe /tmp/$exe
  echo -n "BEGIN TIME: "; date
  run_milc
  echo -n "END TIME: "; date
fi

