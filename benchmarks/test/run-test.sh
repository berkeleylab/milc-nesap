#!/bin/bash
#SBATCH --account=m888
#SBATCH --partition=debug
#SBATCH --constraint="haswell"
#SBATCH --nodes=1
#SBATCH --time=00:10:00
#SBATCH --job-name=milc
#SBATCH --wait-all-nodes=1
nodes=$SLURM_JOB_NUM_NODES
echo "Running 16x16x16x16 lattice on $nodes"

exe=su3_rhmd_hisq

n=8			# MPI ranks per node
./run_milc.sh -n $n -nx 16 -ny 16 -nz 16 -nt 16 --exe $exe --brief
