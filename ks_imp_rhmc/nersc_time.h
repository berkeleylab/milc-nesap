// Header file for NERSC timing and profiling
#ifndef _NERSC_TIME_H
#define _NERSC_TIME_H

#include <stdlib.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <limits.h>
#ifdef PROF_VTUNE
#  include <ittnotify.h>
#endif

// These globals are defined in control.c
extern int    nersc_stanza; // counts the number of "stanzas" in the configuration input file
extern double t_total;      // total time spent in MILC
extern double t_congrad;    // time spent in congugate gradient
extern double t_fforce;     // time spent in fermion force calculation
extern double t_gforce;     // time spent in gauge force calculation
extern int    nersc_steps;  // counts the number of Omelyan integration steps
extern int    nersc_stop;   // step exit threshold set by envrion variable NERSC_NUM_STEPS

static void nersc_time_report() {
  struct rusage usage;
  if(this_node==0){
    if (nersc_stop != INT_MAX) {
      printf("---> NERSC PROFILING REGION END: profiled steps = %d\n", nersc_steps);
      printf("---> Except for TOTAL_TIME, reports are for PROFILED REGION only\n");
    }
    printf("TOTAL_TIME    %.3f secs\n", t_total);
    printf("CG_TIME       %.3f secs\n", t_congrad);
    printf("FF_TIME       %.3f secs\n", t_fforce);
    printf("GF_TIME       %.3f secs\n", t_gforce);
    if (getrusage(RUSAGE_SELF, &usage) == 0)
      printf("Approximate memory usage = %.3f MB\n", (float)usage.ru_maxrss*numnodes()/1000.0);
  }
}
#endif // _NERSC_TIME_H
